<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserRegisteredMail;
use Mail;

class SendOtpUserRegistered implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredEvent  $event
     * @return void
     */
    
     public function handle($event)
    {
        if ($event->condition == 'register'){
            $pesan = "We're excited to have You to get started. First, you need to confirm your account. This is your OTP Code : ";
        }
        elseif ($event->condition == 'regenerate'){
            $pesan = "Regenerate OTP succesful. This is Your OTP Code : ";
        }

        Mail::to($event->user)->send(new UserRegisteredMail($event->user, $pesan));
    }
}
